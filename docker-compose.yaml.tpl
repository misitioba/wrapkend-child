version: "3"
services:
  app:
      build: .
      image: misitioba/wrapkend-container
      container_name: __container__
      volumes:
        - ./:/app
      working_dir: /app
      env_file: ./.env
      ports:
        - __port__:__port__
      command: npm run docker
      restart: always
networks:
  default:
    external:
      name: webproxy
